function counterFactory(){
    let counter = 0;
    obj = {
        increment : function(){
            counter = counter + 1
            return counter
        },
        decrement : function(){
            counter = counter - 1
            return counter
        }
    }
    return obj
}

module.exports = counterFactory