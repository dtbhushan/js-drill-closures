const counterFactory = require("../counterFactory")

result = counterFactory()
value = result.increment()
console.log(value)
value = result.decrement()
console.log(value)