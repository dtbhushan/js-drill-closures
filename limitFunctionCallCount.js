function limitFunctionCallCount(cb, n){
    var mainCount = n
    function returnedFunction(){
        for (let count = 0; count < mainCount; count++){
            cb()
        }
    }
    return returnedFunction
}
module.exports = limitFunctionCallCount